import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class CheckAPI {
  final httpClient = HttpClient();

  CheckAPI() {
    //_init();
  }

  Future<String> call(String url) async {
    return httpClient
        .getUrl(Uri.parse(url))
        .then((HttpClientRequest request) {
          // Optionally set up headers...
          // Optionally write to the request object...
          // Then call close.
          return request.close();
        })
        .then(_readResponse)
        .then((body) {
          return (body);
        });
  }

  Future _readResponse(HttpClientResponse response) {
    if (response.statusCode == 200) {
      var completer = new Completer();
      var contents = new StringBuffer();
      response.transform(utf8.decoder).listen((String data) {
        contents.write(data);
      }, onDone: () => completer.complete(contents.toString()));
      return completer.future;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}

class Preferences {
  Future<bool> save(key, value) async {
    //SharedPreferences.setMockInitialValues({});
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(key, value);
  }

  Future<String> fetch(key) async {
    //SharedPreferences.setMockInitialValues({});
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}
