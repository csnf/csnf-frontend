import 'dart:convert';
import 'dart:io';

import 'package:check1_5/api.dart';
import 'package:check1_5/productView.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb && (Platform.isMacOS || Platform.isWindows || Platform.isLinux)) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
}

void main() {
  _enablePlatformOverrideForDesktop();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Check 1.5°',
      theme: ThemeData(
          primarySwatch: Colors.lightGreen,
          fontFamily: 'Katwijk',
          colorScheme: ColorScheme(
              primary: Colors.lightGreen,
              secondary: Colors.pink,
              primaryVariant: Colors.tealAccent,
              secondaryVariant: Colors.pinkAccent,
              surface: Colors.white,
              background: Colors.white,
              error: Colors.red,
              onPrimary: Colors.black,
              onSecondary: Colors.black,
              onSurface: Colors.black,
              onBackground: Colors.black,
              brightness: Brightness.light,
              onError: Colors.white),
          textTheme: TextTheme(
            headline:
                TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            subhead: TextStyle(fontFamily: 'Klima'),
            title: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            button: TextStyle(fontFamily: 'Klima'),
            display1:
                TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            overline: TextStyle(fontFamily: 'Klima'),
            display2:
                TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            display3:
                TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            display4:
                TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            subtitle:
                TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w800),
            caption: TextStyle(fontFamily: 'Klima'),
          )),
      home: MyHomePage(title: 'Check Your Product'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  String _ourCode = "Nothing...";
  String _historyString = "";

  @override
  Widget build(BuildContext context) {
    Widget body;
    switch (_selectedIndex) {
      case 0:
        {
          body = Center(
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'Welcome to our application.\nWe wanna improve the world by providing you information about the products you´re buying.',
                    style: Theme.of(context).textTheme.body2,
                  ),
                ),
                FloatingActionButton.extended(
                  icon: Icon(FontAwesomeIcons.cameraRetro),
                  label: Text(
                    "Scann!",
                  ),
                  //style: Theme.of(context).textTheme.display2,),
                  onPressed: scanCode,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'Code: $_ourCode',
                  ),
                )
                /* Text(
                                          '$_counter',
                                          style: Theme.of(context).textTheme.display1,
                                        ), */
              ],
            ),
          );
          break;
        }
      case 1:
        {
          var eanController=TextEditingController();
          body = Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                      TextField(
                        controller: eanController,
                        decoration: InputDecoration(
                          labelText: "Produkt search",
                        ),
                        keyboardType: TextInputType.text,
                      ),
                      RaisedButton(child: Text("Search"), onPressed: (){
                        showProductDetails(eanController.value);
                      },)
                  /* Text(
                        '$_counter',
                        style: Theme.of(context).textTheme.display1,
                      ), */
                ],
              ),
            ),
          );
          break;
        }
      case 2:
        {
          body = ProductView(
            barcode: _ourCode,
          );
          break;
        }
      case 3:
        {
          var prefs = Preferences();
          Widget tile;
          if (_historyString == "") {
            prefs.fetch("history").then((historyString) {
              setState(() {
                _historyString = historyString;
              });
            });
            tile = CircularProgressIndicator();
          } else {
            var history = jsonDecode(_historyString);
            tile = ListView.separated(
                itemCount: history.length,
                separatorBuilder: (BuildContext context, int index) => Divider(),
                itemBuilder: (context, index) {
                  return (ListTile(
                    onTap: (){
                      _selectedIndex = 2;
                      setState(() {
                        _ourCode = history[index];
                      });
                    },
                    title: Text(history[index]),
                  ));
                });
          }
          body = tile;

          break;
        }
    }

    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset("assets/jh_logo.png"),
        ),
        title: Text(widget.title),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.barcode),
            title: Text('Scan'),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.search),
            title: Text('Search'),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.infoCircle),
            title: Text('Infos'),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.history),
            title: Text('Feed'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.teal,
        unselectedItemColor: Colors.pinkAccent,
        backgroundColor: Color.fromRGBO(170, 211, 123, 1),

        onTap: _onItemTapped,
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/environment.png"),
              fit:BoxFit.contain,
                alignment: Alignment.bottomCenter,
            )
          ),
          child: body),
    );
  }

  void _onItemTapped(int value) {
    setState(() {
      _selectedIndex = value;
    });
  }

  Future<void> scanCode() async {
    String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#808080', 'cancel', true, ScanMode.BARCODE);
    showProductDetails(barcodeScanRes);
  }

  Future<void> showProductDetails(eanId) async {
    var prefs = Preferences();
    prefs.fetch("history").then((historyText) {
      List history;
      try {
        history = jsonDecode(historyText);
        history.add(eanId);
      } catch (e) {
        history = List();
        history.add(eanId);
      }
      historyText = jsonEncode(history);
      prefs.save("history", historyText).then((succeed) {
        /*Scaffold.of(context).showSnackBar(SnackBar(
          content: Text((succeed)
              ? "Barcode was saved to history."
              : "Error saving barcode."),
        ));*/
      });
    });
    _selectedIndex = 2;
    setState(() {
      _ourCode = eanId;
    });
  }
}
