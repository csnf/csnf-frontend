import 'package:datakick_sdk/datakick_sdk.dart';
import 'package:flutter/material.dart';

class ProductBarcodeWidget extends StatefulWidget {
  final String barcode;

  ProductBarcodeWidget({Key key, this.barcode}) : super(key: key);

  @override
  _ProductBarcodeWidgetState createState() => _ProductBarcodeWidgetState();
}

class _ProductBarcodeWidgetState extends State<ProductBarcodeWidget> {
  bool _productLoaded;
  Product _product = new Product.empty();

  @override
  Widget build(BuildContext context) {
    final String barcode = widget.barcode;
    Widget child;

    /// Then pass a barcode to the object and wait for the future
    if (!this._productLoaded) {
      _product.getBarcode(barcode).then((product) {
        print(product);
        _product = product;
        setState(() {
          _productLoaded = true;
        });
      });
      child = Container(
        width: 56,
        height: 56,
        child: CircularProgressIndicator(),
      );
    } else {
      child = Container(
        width: 300,
        height: 300,
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text(
                _product.name.toString(),
                style: Theme.of(context).textTheme.display1,
              ),
            )
          ],
        ),
      );
    }

    return (Card(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: child,
      ),
    ));
  }
}
