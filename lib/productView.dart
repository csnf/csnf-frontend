import 'dart:convert';

import 'package:check1_5/api.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProductView extends StatefulWidget {
  final String barcode;

  ProductView({Key key, this.barcode}) : super(key: key);

  @override
  _ProductViewState createState() => _ProductViewState();
}

class _ProductViewState extends State<ProductView> {
  bool _productLoaded = false;
  String json = "";
  Map _product = Map();
  String _server = "94.45.228.239:8070";
  TextEditingController _ipController=TextEditingController();

  //TextEditingController _ipController=TextEditingController()

  @override
  Widget build(BuildContext context) {
    CheckAPI api = CheckAPI();
    final String barcode = widget.barcode;
    Widget child;
    /*Preferences prefs=Preferences();
    if(_server!=null) prefs.fetch("serverIp").then((ip){
      setState(() {
        _server=ip;
      });
    });*/

    /// Then pass a barcode to the object and wait for the future
    if (!this._productLoaded) {
      api
          .call("http://" + _server + "/check1.5/barcode/find/" + barcode)
          .then((data) {
        _product = jsonDecode(data);
        setState(() {
          _productLoaded = true;
        });
      });
      child = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(16),
            child: TextField(
              controller: _ipController,
              decoration: InputDecoration(
                  labelText: "Server IP", helperText: "hostname:port"),
              onChanged: (newValue) {
                setState(() {
                  _server = newValue;
                });
              },
            ),
          ),
          Container(
            width: 56,
            height: 56,
            child: CircularProgressIndicator(),
          )
        ],
      );
    } else {
      child = Container(
        width: 300,
        height: 600,
        child: ListView.builder(
          itemCount: _product.keys.length,
          itemBuilder: (context, index) {
            Widget tile;
            var keyList = List.from(_product.keys);
            var key = keyList[index];
            if (key == "score") {
              var score = _product[key];
              List<Widget> scoreRow = [];
              while (score >= 1) {
                scoreRow.add(Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    FontAwesomeIcons.solidStar,
                    color: Colors.green,
                  ),
                ));
                score--;
              }
              if ((score - 0.5) >= 0) {
                scoreRow.add(Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    FontAwesomeIcons.starHalfAlt,
                    color: Colors.green,
                  ),
                ));
              }
              while (scoreRow.length <5) {
                scoreRow.add(Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    FontAwesomeIcons.star,
                    color: Colors.green,
                  ),
                ));
                score--;
              }
              tile=(ListTile(
                subtitle: Text(
                  "Check 1.5° Score: "+_product[key].toString(),
                  style: Theme.of(context).textTheme.subtitle,
                ),
                title: Row(
                  children: scoreRow,
                ),
              ));
            }
            else tile=(ListTile(
              subtitle: Text(
                key,
                style: Theme.of(context).textTheme.subtitle,
              ),
              title: Text(
                _product[key].toString(),
                style: Theme.of(context).textTheme.title,
              ),
            ));
            return tile;
          },
        ),
      );
    }

    return ListView(children: [
      child,
    ]);
  }
}
